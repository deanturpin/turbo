#include "tokenise.h"

#include <cassert>
#include <iomanip>

// Tokenise the expression
std::vector<std::string> tokenise(const std::string &expression) {
  std::stringstream ss(expression);
  std::string token;
  std::vector<std::string> tokens;

  auto e = expression;

  while (!e.empty()) {

    size_t j = e.find_first_of("+-x*/");

    if (j == std::string::npos) {
      tokens.push_back(e);
      break;
    }

    const size_t jump = std::max(1ul, j);
    auto tk = e.substr(0, jump);
    tokens.push_back(tk);

    e = e.substr(jump);
  }

  return tokens;
}

#include "gtest/gtest.h"
TEST(test_tokenise, count_result_tokens) {

  EXPECT_EQ(tokenise("4").size(), 1);
  EXPECT_EQ(tokenise("444+4").size(), 3);
  EXPECT_EQ(tokenise("444-7774").size(), 3);
  EXPECT_EQ(tokenise("444+4-5").size(), 5);
}
