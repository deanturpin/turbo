#include "evaluate.h"

#include "gtest/gtest.h"
TEST(test_turbo, basic_operations) {
  EXPECT_EQ(eval("4"), "4");
  EXPECT_EQ(eval("x"), "");
  EXPECT_EQ(eval("-"), "");
  EXPECT_EQ(eval(""), "");
  EXPECT_EQ(eval("1 + 2"), "3");
  EXPECT_EQ(eval("100 + 200"), "300");
  EXPECT_EQ(eval("2 - 1"), "1");
  EXPECT_EQ(eval("10 * 2"), "20");
  EXPECT_EQ(eval("1001 x 1002"), "1003002");
  EXPECT_EQ(eval("1,000,000 x 2"), "2000000");
}

TEST(test_turbo, divide) {
  EXPECT_EQ(eval("10 / 2"), "5");
  EXPECT_EQ(eval("10 / 20"), "0");
}
