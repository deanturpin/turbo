#include "evaluate.h"
#include <iostream>

int main() {

  std::cout << "turbo ftw" << std::endl;

  // Process each line as an expression
  while (true) {
    std::string expression;
    std::getline(std::cin, expression);

    // Quit if we must
    if (expression == "quit" || expression == "q") {
      std::cout << "turbo ftw" << std::endl;
      break;
    }

    // Otherwise evaluate the expression
    std::cout << eval(expression) << "\n";
  }
}
