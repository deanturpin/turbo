cmake_minimum_required(VERSION 3.16)

project(turboscript)

enable_testing()
include(GoogleTest)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

add_library(turbo turbo.cxx clean.cxx tokenise.cxx evaluate.cxx)

add_executable(demo demo.cxx)
target_link_libraries(demo turbo gtest gtest_main pthread)

add_executable(test-turbo turbo.cxx)
target_link_libraries(test-turbo turbo gtest gtest_main pthread)

gtest_discover_tests(test-turbo)

