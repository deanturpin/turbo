#pragma once

#include <string>

// Remove spaces and punctuation
std::string clean_expression(const std::string &);
