#include "evaluate.h"
#include "clean.h"
#include "tokenise.h"

#include <cassert>
#include <charconv>
#include <functional>
#include <string_view>

// Convert back and forth to a double to validate token
std::optional<int> get_as_number(const std::string &token) {
  const std::string_view original_string = token;

  int to_a_number = 0;
  std::from_chars(original_string.begin(), original_string.end(), to_a_number);

  return original_string == std::to_string(to_a_number) ?
    to_a_number : std::optional<int>{};
}

// Evaluate a container of tokens
std::string evaluate(const std::vector<std::string> &tokens) {

  const size_t number_of_tokens = tokens.size();

  // If only one value, just return it
  if (number_of_tokens == 1)
    return get_as_number(tokens.front()) ? tokens.front() : std::string{};

  // Otherwise break down expression
  if (number_of_tokens == 3) {

      const auto _lhs = get_as_number(tokens.front());
      const auto _rhs = get_as_number(tokens.back());

    if (_lhs && _rhs) {

	const int lhs = _lhs.value();
	const int rhs = _rhs.value();

      const std::string operation = tokens.at(1);
      if (operation == "+")
        return std::to_string(std::plus{}(lhs, rhs));

      else if (operation == "-")
        return std::to_string(std::minus{}(lhs, rhs));

      else if (operation == "*" || operation == "x")
        return std::to_string(std::multiplies{}(lhs, rhs));

      else if (operation == "/")
        return std::to_string(std::divides{}(lhs, rhs));
    }
  }

  return {};
}

// Evaluate an expression string
std::string eval(const std::string &unclean) {
  return evaluate(tokenise(clean_expression(unclean)));
}

#include "gtest/gtest.h"

TEST(test_evaluate_tokens, tokens) {
  EXPECT_EQ(evaluate({"3", "x", "2"}), "6");
  EXPECT_EQ(evaluate({"10", "-", "5"}), "5");
  EXPECT_EQ(evaluate({"3", "+", "2"}), "5");
  EXPECT_EQ(evaluate({"100000", "-", "99999"}), "1");
  EXPECT_EQ(evaluate({"3", "*", "2"}), "6");
}

TEST(test_turbo, break_down_single_expression) {
  EXPECT_EQ(clean_expression("1,000 + 2"), "1000+2");
  EXPECT_EQ(tokenise("1000+2").size(), 3ul);
  EXPECT_EQ(evaluate({"1000", "+", "2"}), "1002");
  EXPECT_EQ(eval({"  1,000   +   2   "}), "1002");
}

TEST(test_turbo, trimming) {
  EXPECT_EQ(eval({"  1,000   +   2   "}), "1002");
  EXPECT_EQ(eval({"  1,000   -   2   "}), "998");
  EXPECT_EQ(eval({"  1,000   x   2   "}), "2000");
  EXPECT_EQ(eval({"  1,000   *   2   "}), "2000");
  EXPECT_EQ(eval({"  1,000   /   2   "}), "500");
}

TEST(validate_as_number, basic) {
  EXPECT_EQ(get_as_number("5"), 5l);
  EXPECT_FALSE(get_as_number(""));
  EXPECT_FALSE(get_as_number("------------------------"));
  EXPECT_EQ(get_as_number("-5"), -5l);
  EXPECT_FALSE(get_as_number("x").has_value());
  EXPECT_FALSE(get_as_number("\n").has_value());
}

