#include "clean.h"

#include <algorithm>
#include <cassert>
#include <cctype>
#include <iterator>

// Remove spaces and punctuation
std::string clean_expression(const std::string &expression) {
  std::string cleaned;

  std::copy_if(expression.cbegin(), expression.cend(),
               std::back_inserter(cleaned),
               [](const auto &c) { return c != ',' && !std::isspace(c); });

  return cleaned;
}

#include "gtest/gtest.h"
TEST(test_clean, extra_chars) {

  EXPECT_EQ(clean_expression("1,000"), "1000");
  EXPECT_EQ(clean_expression(",,,,,,5,,,,,,"), "5");
}

TEST(test_clean, whitespace) {

  EXPECT_EQ(clean_expression(""), "");
  EXPECT_EQ(clean_expression("          "), "");
  EXPECT_EQ(clean_expression("  3 + 4   "), "3+4");
}
