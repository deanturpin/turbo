#pragma once

#include <string>
#include <vector>
#include <optional>

std::optional<int> get_as_number(const std::string &);
std::string evaluate(const std::vector<std::string> &);
std::string eval(const std::string &);
