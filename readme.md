# About
Exploring options to allow non-softies to define the logic of compiled code.

# Build and run
```
apt update
apt install --yes cmake build-essential
cmake -S src -B bin
cmake --build bin
bin/turbo
```

# TODO
-[x] Interactive demo
-[ ] Catch
- Negative numbers
- Doubles
- Heap implementation for logic: https://en.cppreference.com/w/cpp/algorithm/make_heap
- Is binary tree better than a heap? doesn't need to be balanced
- Operator precedence - simple (left to right)
- Operator precedence - full
- Brackets
- Also offer $1 instead of named vars?
- XML parser that keeps the string in memory and adds accessors
- Parse text file in a static lambda
- Refactor validate/get as number
- Remove leading and trailing whitespace with string_view remove_prefix
- Clean control chars

## Rename
- Arithmescript
- mukeSH

# Static/compile time lambda
```c++
const auto func = [file = "func.yaml"](start, end) {
    return true;

};
```

# Formats
- Uppercase or prefix tokens with special character, will be substitued by the code
- Compiled exection times with logic defined in a text file
- XSL - https://www.brainbell.com/tutorials/XML/Putting_Expressions_To_Work.htm
```xsl
<xsl:if test="countdown &lt;= 0">
  Lift off!
</xsl:if>
```
- math markdown parser: can render as formula

# Unit testing frameworks
- cxxtest/groovy 4.4+git171022-2
- libcppunit-1.15-0/groovy 1.15.1-2build1 amd64 Unit Testing Library for C++

